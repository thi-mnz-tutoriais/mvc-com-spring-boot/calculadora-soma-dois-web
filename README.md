# calculadora-soma-dois-web

Calculadora que realiza as operações aritméticas básicas entre dois números.

Este projeto visa demonstrar a separação básica das camadas Model, View e Controller de maneira simplificada.