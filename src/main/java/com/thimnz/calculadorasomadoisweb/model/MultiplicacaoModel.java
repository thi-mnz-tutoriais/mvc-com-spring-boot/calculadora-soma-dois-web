package com.thimnz.calculadorasomadoisweb.model;

public class MultiplicacaoModel {
	
	private double v1;
	private double v2;
	
	public MultiplicacaoModel(double v1, double v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	public double getV1() {
		return v1;
	}

	public void setV1(double v1) {
		this.v1 = v1;
	}

	public double getV2() {
		return v2;
	}

	public void setV2(double v2) {
		this.v2 = v2;
	}

	public double getResultado() {
		return v1 * v2;
	}
}
