package com.thimnz.calculadorasomadoisweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadoraSomaDoisWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadoraSomaDoisWebApplication.class, args);
	}

}
