package com.thimnz.calculadorasomadoisweb.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

import com.thimnz.calculadorasomadoisweb.dto.OperacaoDTO;

public interface IOperacoesAritmeticas {
	public Double getResultadoDaOperacaoEntreDoisNumeros(double valorUm, double valorDois);
	
	@ModelAttribute("operacaoDTO")
	public default OperacaoDTO getNewOperacaoDTO() {
		return new OperacaoDTO();
	}
}
