package com.thimnz.calculadorasomadoisweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thimnz.calculadorasomadoisweb.dto.OperacaoDTO;
import com.thimnz.calculadorasomadoisweb.model.DivisaoModel;

@Controller
@RequestMapping("/divisao")
public class DivisaoController implements IOperacoesAritmeticas {

	private ModelAndView view;
	
	@Override
	public Double getResultadoDaOperacaoEntreDoisNumeros(double valorUm, double valorDois) {
		return new DivisaoModel(valorUm, valorDois).getResultado();
	}
	
	@GetMapping
	public ModelAndView main() {
		view = new ModelAndView("divisao");
		return view;
	}
	
	@PostMapping
	public ModelAndView resultado(OperacaoDTO obj, RedirectAttributes redirectAttributes) {
		view = new ModelAndView("divisao");
		Double resultado = this.getResultadoDaOperacaoEntreDoisNumeros(obj.getValorUm(), obj.getValorDois());

		if(Double.isFinite(resultado) && !Double.isNaN(resultado)) {
			view.addObject("resultado", this.getResultadoDaOperacaoEntreDoisNumeros(obj.getValorUm(), obj.getValorDois()));
		} else {
			redirectAttributes.addFlashAttribute("erro", "Divisão inválida!");
			view = new ModelAndView("redirect:/divisao");
		}
		
		return view;
	}

}
