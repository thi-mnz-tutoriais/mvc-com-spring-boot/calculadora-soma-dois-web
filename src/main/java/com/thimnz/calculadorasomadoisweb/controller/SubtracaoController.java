package com.thimnz.calculadorasomadoisweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.thimnz.calculadorasomadoisweb.dto.OperacaoDTO;
import com.thimnz.calculadorasomadoisweb.model.SubtracaoModel;

@Controller
@RequestMapping("/subtracao")
public class SubtracaoController implements IOperacoesAritmeticas {

	private ModelAndView view;
	
	@Override
	public Double getResultadoDaOperacaoEntreDoisNumeros(double valorUm, double valorDois) {
		return new SubtracaoModel(valorUm, valorDois).getResultado();
	}
	
	@GetMapping
	public ModelAndView main() {
		view = new ModelAndView("subtracao");
		return view;
	}
	
	@PostMapping
	public ModelAndView resultado(OperacaoDTO obj) {
		view = new ModelAndView("subtracao");
		view.addObject("resultado", this.getResultadoDaOperacaoEntreDoisNumeros(obj.getValorUm(), obj.getValorDois()));
		return view;
	}
}
