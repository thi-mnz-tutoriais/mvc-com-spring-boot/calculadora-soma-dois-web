package com.thimnz.calculadorasomadoisweb.dto;

public class OperacaoDTO {
	
	private double valorUm;
	private double valorDois;
	private double resultado;
	
	public OperacaoDTO() {
		
	}
	
	public OperacaoDTO(double valorUm, double valorDois) {
		this.valorUm = valorUm;
		this.valorDois = valorDois;
	}
	
	public OperacaoDTO(double resultado) {
		this.resultado = resultado;
	}
	
	public OperacaoDTO(double valorUm, double valorDois, double resultado) {
		this.valorUm = valorUm;
		this.valorDois = valorDois;
		this.resultado = resultado;
	}
	
	public double getValorUm() {
		return valorUm;
	}
	public void setValorUm(double valorUm) {
		this.valorUm = valorUm;
	}
	public double getValorDois() {
		return valorDois;
	}
	public void setValorDois(double valorDois) {
		this.valorDois = valorDois;
	}
	public double getResultado() {
		return resultado;
	}
}
